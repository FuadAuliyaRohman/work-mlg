@extends('layoutt.master-template')

@section('title')
Komentar
@endsection

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="card">

                    <div class="card-header">
                        <h3 class="card-title">Kelola Forum</h3>
                    </div>
                    <br />
                <div class="col-md-10 offset-md-1 ">
                    @can("komentar-export")
                    <a href="{{route('reportcomen')}}" class="btn btn-info"><i class="far fa-file-excel"></i>
                Export exel</a>
                @endcan
                </div>
                    {{-- <a href="{{route('user/export')}}" class="btn btn-success"><i class="far fa-file-excel"></i>
                    exel</a> --}}
                    <!-- /.card-header -->

                    <div class="card-body">
                        @include('alert.success')
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>User</th>
                                    <th>Komentar</th>
                                    <th>Dokumen</th>
                                    <th style="text-align:center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($forum as $row)
                                <tr>
                                    <td>{{ $loop->iteration + ($forum->perPage() * ($forum->currentPage() - 1)) }}
                                    </td>
                                    <td>{{$row->user->full_name}}</td>
                                    <td>{{ $row->message }}</td>
                                    @if($row->documen)
                                    <td>{{ $row->documen->judul_dokumen}}</td>
                                    @else
                                    <td>Upss belum diisi</td>
                                    @endif
                                    <td style="text-align:center">
                                       
                                            {{-- <a class="btn btn-round btn-warning btn-md far fa-edit" href="{{ route('document.edit',[$row->id]) }}"></a>
                                            --}}
                                            @can("komentar-delete")
                                           <button type="submit" class="btn btn-round btn-warning fas fa-trash-alt komen"
                                                data-id="{{ $row->id  }}"></i></button>
                                                @endcan
                                            {{-- <form method="post" action="">
                                                                                            @csrf
                                                                                            {{method_field('DELETE')}}
                                            <a href="" class="btn btn-round btn-warning btn-md"><i class="fa fa-edit"
                                                    data-toggle="modal" data-target="#edit"></i></a> --}}
                                       
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    <tfoot >
                    <nav aria-label="Page navigation example">
                        <ul class="pagination justify-content-center">
                           {{ $forum->appends(Request::all())->links() }}
                        </ul>
                    </nav>
                 </tfoot>
                    </div>

                </div>
                <!-- /.card-body -->
            </div>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
    </div>
    </div>
    <!-- /.container-fluid -->
</section>
<!-- /.content -->


<script>
    $(function () {
    $('#example2').DataTable({
    "paging": false,
    "lengthChange": true,
    "searching": true,
    "ordering": true,
    "info": true,
    "autoWidth": false,
    "responsive": true,
    
    });
  });
  
</script>

<script>
    $(function () {
      $('.select2').select2()
    });
</script>
@push('addon-script')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
    $('.komen').click( function(){
                  var komentar = $(this).attr('data-id');
                  swal({
                      title: "Yakin?",
                      text: "Anda akan menghapus data",
                      icon: "warning",
                      buttons: true,
                      dangerMode: true,
                      })
                      .then((willDelete) => {
                      if (willDelete) {
                          window.location = "/komen/"+komentar+"" 
                          swal("Data berhasil dihapus", {
                          icon: "success",
                          });
                      } else {
                          swal("Data Tidak Jadi dihapus");
                      }
                      });
              })
                          
</script>
@endpush

@endsection