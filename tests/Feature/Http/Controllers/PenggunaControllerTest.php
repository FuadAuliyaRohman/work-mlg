<?php

namespace Tests\Feature\Http\Controllers;

use Tests\TestCase;
use App\Models\User;
use App\Models\status;
use Tests\MigrateFreshSeedOnce;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Artisan;
use Spatie\Permission\Models\Permission;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class PenggunaControllerTest extends TestCase
{
    /** @test */
    public function login()
    {
        $response = $this->get('/');
        $response->assertStatus(200);
    }

    /** @test */
    public function login_admin_dashboard()
    { 
        $response = $this->post('/login', [
            'username' => 'superadmin',
            'password' => 'superadmin',
        ]);

        $this->withoutExceptionHandling();
        $response->assertRedirect('/dashboard');
    }

    /** @test */
    public function login_user_dashboard()
    { 
        $response = $this->post('/login', [
            'username' => 'andi',
            'password' => 'andi',
        ]);

        $this->withoutExceptionHandling();
        $response->assertRedirect('/dashboardUser');
    }

    /** @test */
/*     public function lihat_user()
    { 
        $response = $this->get('/user');
        $response->assertStatus(200);
    } */
}