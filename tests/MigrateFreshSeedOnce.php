<?php
namespace Tests;
use Illuminate\Support\Facades\Artisan;

trait MigrateFreshSeedOnce
{
    /**
    * If true, setup has run at least once.
    * @var boolean
    */
    protected static $setUpHasRunOnce = false;
    /**
    * After the first run of setUp "migrate:fresh --seed"
    * @return void
    */
    public function setUp()
    {
        parent::setUp();
        if (!static::$setUpHasRunOnce) {
            Artisan::call('migrate:fresh --seeder');
            /* Artisan::call(
                'db:seed', ['--class' => 'UserSeeder']
            ); */
            static::$setUpHasRunOnce = true;
        }
    }
}