<?php

namespace Tests;

use Faker\Factory as Faker;
use Tests\MigrateFreshSeedOnce;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    //MigrateFreshSeedOnce;

/*     protected $faker; */

    /**
     * Setup Test
     *
     * @return void
     */
/*     public function setUp(): void
    {
        parent::setUp();

        $this->faker = Faker::create();
    } */

    /**
     * TearDown
     *
     * @return void
     */
/*     public function tearDown(): void
    {
        parent::tearDown();
    } */
}
