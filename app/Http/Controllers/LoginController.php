<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Slider;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Permission;

class LoginController extends Controller
{
    public function index(Role $joblist){
        // $slider = Slider::paginate(10);
        $roles = Role::get();
        // $roles = Role::find($joblist);
        // dd($roles);
        return view('general.login',['roles'=>$roles]);  
    }
}
