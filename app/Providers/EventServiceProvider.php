<?php

namespace App\Providers;

use Aacotroneo\Saml2\Events\Saml2LoginEvent;
use Aacotroneo\Saml2\Events\Saml2LogoutEvent;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Session;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        Event::listen(Saml2LoginEvent::class, function (Saml2LoginEvent $event) {
            //  return $event;

            //dd($event);
            $messageId = $event->getSaml2Auth()->getLastMessageId();
            // Add your own code preventing reuse of a $messageId to stop replay attacks

            $user = $event->getSaml2User();
            //dd($user);
            $userData = [
                'id' => $user->getUserId(),
                'attributes' => $user->getAttributes(),
                'assertion' => $user->getRawSamlAssertion()
            ];
            //dd($userData, $user);
            //dd($userData);

            $appUser = User::where('username', $userData['attributes']['employee_no'][0])->first();

            //dd($appUser,$userData);
            if ($appUser == NULL) {
                //return redirect()->intended('dashboardUser');
                return abort(403, 'Anda tidak punya akses akun anda belum terdaftar di reference learning');
            }
            //dd($appUser);
            // if(is_null($userData)){
            //     return 'user not found';
            // }
            // if(is_null($user)){
            //     return 'user not found';
            // }
            if ($appUser) {
                Auth::loginUsingId($appUser->id);
            }
            // else{
            //     $user = new User;
            //     $user->username =$userData['attributes']['employee_no'][0];
            //     return redirect()->back()->with('failmessage', 'Username/Password anda salah!');
            // }

            // Auth::login($appUser);
            //return redirect()->back();
        });

        Event::listen(Saml2LogoutEvent::class, function ($event) {
            //return $event;
            Auth::logout();
            //return redirect()->intended('/dashboardUser');
            Session::save();
        });
    }
}
