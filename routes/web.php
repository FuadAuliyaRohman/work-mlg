<?php

use Illuminate\Support\Facades\Route;

// Route::middleware('example-saml-auth')->group(function () {
//     Route::name('logout')->post('logout', '\Aacotroneo\Saml2\Http\Controllers\Saml2Controller@logout');
// });
// Route::redirect('about-page', 'about');

Route::middleware('example-saml-auth')->group(function () {
    Route::name('logout')->post('logout', '\Aacotroneo\Saml2\Http\Controllers\Saml2Controller@logout');

});

Route::namespace('Auth')->group(function () {
    Route::post('login', 'LoginController@authenticate')->name('login');
    Route::get('logout', 'LoginController@logout')->name('logout');
    // Route::name('logout')->post('logout', '\Aacotroneo\Saml2\Http\Controllers\Saml2Controller@logout');
});

    Route::group(['middleware' => 'App\Http\Middleware\Authenticate'], function () {
        Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
    Route::get('/dashboardUser', 'DashboardUserController@index')->name('dashboardUser');
    Route::get('/dashboardShow-{id}', 'DashboardUserController@show')->name('dashboardShow');

    //Route::get('/dashboardUser-{id}', 'dashboardUserController@show')->name('dashboard');
    Route::get('file/download/{file}', 'kumpulanEbookController@download')->name('download');
    Route::resource('/user', 'UserController');
    Route::get('/profil', 'ProfilController@index')->name('profil.index');
    Route::post('/update-profil/{id}', 'ProfilController@update')->name('update.profil');
    Route::match(['get', 'post'], '/edit/{id}', 'KategoriController@edit');
    //Route::match(['get', 'post'], '/edit/{id}', 'sliderController@edit');
    Route::resource('/document', 'KelolaDocumentController');
    Route::get('/delete/{id}', 'KelolaDocumentController@delete')->name('delete');
    Route::get('/hapus/{id}', 'DocumentController@hapus')->name('hapus');
    Route::get('/buang/{id}', 'KategoriController@buang')->name('buang');
    Route::get('/komen/{id}', 'ForumController@komen')->name('komen');
    Route::get('/buku/{id}', 'AdminEbookController@buku')->name('buku');
    Route::get('/bukuadmin/{id}', 'EbookController@bukuadmin')->name('bukuadmin');
    Route::get('/usersdel/{id}', 'UserController@usersdel')->name('usersdel');
    Route::resource('/doc', 'DocumentController');
    Route::resource('/kategori', 'KategoriController')->except(['destroy']);;
    Route::resource('/like', 'MyLikeController');
    Route::resource('/reference', 'MyReferenceController');
    Route::resource('/forum', 'ForumController');
    Route::resource('/ebook', 'EbookController');
    Route::resource('/adminEbook', 'AdminEbookController');
    Route::resource('/kumpulanEbook', 'KumpulanEbookController');
    Route::post('/documen/{id}', 'MyReferenceController@komentar');
    Route::get('report', 'KelolaDocumentController@report')->name('report');
    Route::get('reportAdmin', 'DocumentController@report')->name('reportAdmin');
    Route::get('reportcomen', 'ForumController@reportc')->name('reportcomen');
    Route::get('export', 'UserController@export')->name('export');
    Route::get('kategoriexport', 'KategoriController@reportkategori')->name('kategoriexport');
    Route::post('import', 'UserController@importexel')->name('import');
    Route::post('importdokumen', 'UserController@importexel')->name('importdokumen');
    Route::post('importkategori', 'KategoriController@import')->name('importkategori');
    Route::post('/likedislike', 'KategoriController@likedislike')->name('likedislike');
    Route::post('/viewadd', 'MyReferenceController@addview')->name('viewadd');
    Route::post('/filter', 'KategoriController@filter')->name('filter');
    Route::get('/preview-{id}', 'PreviewController@index')->name('preview');
    Route::get('/previewdoc-{id}', 'PreviewdocController@index')->name('previewdoc');
    Route::post('/getjudul', 'MyReferenceController@getjudul');
    Route::post('/reference/{id}', 'MyReferenceController@insertData')->name('reference.insertData');
    Route::resource('/gambar', 'GambarController');
    Route::get('locale/{locale}', function ($locale) {
        \Session::put('locale', $locale);
        return redirect()->back();
    });
    });
Route::view('loginn', 'general.loginn');
    // Route::get('/', function () {
    // if( Auth::check() && Auth::user()->hasRole("pusdiklat") ) {
    //             return redirect()->intended('dashboard');
    // }
    // else {
    // return redirect()->intended('dashboardUser');
    // }
    // })->name('home');

// Route::get('/', function () {
//         return redirect('dahsboardUuser');
//     });

// Route::get('/', function () {
//     return view('general.loginn');
// })->name('home');

    

Route::get('/', function () {
    return view('general.loginn');
})->name('home');


// Route::get('/', function () {
//     return redirect('ReferenceLearning');
// })->name('home');;

// Route::view('ReferenceLearning', 'general.login');

// Route::get('/ReferenceLearning', 'LoginController@index')->name('general.login');