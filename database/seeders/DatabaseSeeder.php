<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->command->info('Sukses Import Seeder');

        ini_set('memory_limit', '-1');
        $struktur = 'database/dummy/reference-7.sql';
        DB::unprepared(file_get_contents($struktur));
        
        $this->command->info('Database Dummy Berhsil diimport');
        // \App\Models\User::factory(10)->create();
    }
}
