-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 19 Okt 2021 pada 18.00
-- Versi server: 10.4.8-MariaDB
-- Versi PHP: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `work_mlg`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `document`
--

CREATE TABLE `document` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_kategori` int(10) UNSIGNED NOT NULL,
  `id_forum` bigint(20) DEFAULT NULL,
  `judul_dokumen` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jumlah_halaman` int(11) NOT NULL,
  `deskripsi_dokumen` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `publisher` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tahun` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cover` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tipe_dokumen` bigint(20) DEFAULT NULL,
  `jumlah_like` bigint(20) DEFAULT NULL,
  `jumlah_view` bigint(20) DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `document`
--

INSERT INTO `document` (`id`, `id_kategori`, `id_forum`, `judul_dokumen`, `jumlah_halaman`, `deskripsi_dokumen`, `publisher`, `tahun`, `file`, `cover`, `tipe_dokumen`, `jumlah_like`, `jumlah_view`, `created_by`, `modified_by`, `modified_at`, `created_at`, `updated_at`) VALUES
(7, 14, NULL, 'reference learning', 120, 'What is the meaning of reference? Reference adalah referensi, acuan atau rujukan dalam Bahasa Inggris. Reference biasanya digunakan untuk menulis factual report.', 'wahyu', '2020', 'document/20211003173823/index.html', NULL, NULL, 1, NULL, NULL, NULL, NULL, '2021-10-19 08:50:25', '2021-10-19 08:55:55');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ebook`
--

CREATE TABLE `ebook` (
  `id_ebook` bigint(20) UNSIGNED NOT NULL,
  `id_kategori` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `judul_ebook` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi_ebook` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `publisher` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tahun` int(11) NOT NULL,
  `cover` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_status` int(11) DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `ebook`
--

INSERT INTO `ebook` (`id_ebook`, `id_kategori`, `user_id`, `judul_ebook`, `deskripsi_ebook`, `publisher`, `tahun`, `cover`, `keterangan`, `file`, `id_status`, `created_by`, `modified_by`, `modified_at`, `created_at`, `updated_at`) VALUES
(2, 12, 3, 'buku external', 'Referensi adalah suatu catatan di buku yang memberi tahu dari mana sebuah potongan informasi berasal.\r\n\r\nArtikel ini telah tayang di Kompas.com dengan judul \"Apa itu Reference?\", Klik untuk baca: https://www.kompas.com/skola/read/2020/10/13/120000469/apa-itu-reference-.\r\nPenulis : Risky Guswindari\r\nEditor : Arum Sutrisni Putri\r\n\r\nDownload aplikasi Kompas.com untuk akses berita lebih mudah dan cepat:\r\nAndroid: https://bit.ly/3g85pkA\r\niOS: https://apple.co/3hXWJ0L', 'wahyu', 2020, NULL, NULL, 'ebook/.202-577-2-PB (2).pdf', NULL, NULL, NULL, NULL, '2021-10-19 08:53:42', '2021-10-19 08:53:42'),
(3, 13, 3, 'buku umum', 'Referensi adalah suatu catatan di buku yang memberi tahu dari mana sebuah potongan informasi berasal.\r\n\r\nArtikel ini telah tayang di Kompas.com dengan judul \"Apa itu Reference?\", Klik untuk baca: https://www.kompas.com/skola/read/2020/10/13/120000469/apa-itu-reference-.\r\nPenulis : Risky Guswindari\r\nEditor : Arum Sutrisni Putri\r\n\r\nDownload aplikasi Kompas.com untuk akses berita lebih mudah dan cepat:\r\nAndroid: https://bit.ly/3g85pkA\r\niOS: https://apple.co/3hXWJ0L', 'andi', 2020, NULL, '<p>baik</p>', 'ebook/.CRC CARD.pdf', 1, NULL, NULL, NULL, '2021-10-19 08:54:13', '2021-10-19 08:54:49');

-- --------------------------------------------------------

--
-- Struktur dari tabel `forum`
--

CREATE TABLE `forum` (
  `id` int(10) UNSIGNED NOT NULL,
  `message` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `dokumen_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori`
--

CREATE TABLE `kategori` (
  `id_kategori` int(10) UNSIGNED NOT NULL,
  `kategori_type_id` int(11) NOT NULL,
  `kategori` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jumlah_dokumen` bigint(20) DEFAULT NULL,
  `jumlah_like` bigint(20) DEFAULT NULL,
  `jumlah_view` bigint(20) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_at` datetime NOT NULL DEFAULT current_timestamp(),
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `kategori_type_id`, `kategori`, `jumlah_dokumen`, `jumlah_like`, `jumlah_view`, `created_by`, `modified_by`, `modified_at`, `created_at`, `updated_at`) VALUES
(12, 1, 'Pembelejaran', NULL, NULL, NULL, NULL, NULL, '2021-10-19 01:16:45', '2021-10-18 11:16:45', '2021-10-18 11:16:45'),
(13, 1, 'Peralatan Ketenagalistrikan', NULL, NULL, NULL, NULL, NULL, '2021-10-19 01:16:45', '2021-10-18 11:16:45', '2021-10-18 11:16:45'),
(14, 1, 'Supply Chain Management', NULL, 1, NULL, NULL, NULL, '2021-10-19 01:16:45', '2021-10-18 11:16:45', '2021-10-19 08:55:55'),
(15, 1, 'listrik banyak', NULL, NULL, NULL, NULL, NULL, '2021-10-19 01:16:45', '2021-10-18 11:16:45', '2021-10-18 11:16:45'),
(16, 2, 'pendidikan', NULL, NULL, NULL, NULL, NULL, '2021-10-19 22:55:04', '2021-10-19 08:55:04', '2021-10-19 08:55:04'),
(17, 2, 'K3 pembelajaran', NULL, NULL, NULL, NULL, NULL, '2021-10-19 22:55:29', '2021-10-19 08:55:29', '2021-10-19 08:55:29');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori_type`
--

CREATE TABLE `kategori_type` (
  `kategori_type_id` int(122) NOT NULL,
  `type` varchar(122) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `kategori_type`
--

INSERT INTO `kategori_type` (`kategori_type_id`, `type`, `created_at`) VALUES
(1, 'Teknis', '2021-02-23 07:26:58'),
(2, 'Non Teknis', '2021-02-23 07:27:12');

-- --------------------------------------------------------

--
-- Struktur dari tabel `likesdocument`
--

CREATE TABLE `likesdocument` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `document_id` bigint(20) NOT NULL,
  `modified_at` datetime NOT NULL DEFAULT current_timestamp(),
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `likesdocument`
--

INSERT INTO `likesdocument` (`id`, `user_id`, `document_id`, `modified_at`, `created_at`, `updated_at`) VALUES
(4, 4, 1, '2021-10-19 00:24:00', '2021-10-18 10:24:00', '2021-10-18 10:24:00'),
(5, 4, 2, '2021-10-19 00:24:03', '2021-10-18 10:24:03', '2021-10-18 10:24:03'),
(6, 4, 7, '2021-10-19 22:55:55', '2021-10-19 08:55:55', '2021-10-19 08:55:55');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2021_03_29_063035_create_user_auth', 1),
(2, '2021_03_30_164137_create_status', 1),
(3, '2021_03_30_164253_create_kategori', 1),
(4, '2021_03_30_165415_create_document', 1),
(5, '2021_03_30_170230_create_ebook', 1),
(6, '2021_03_30_170953_create_forum', 1),
(7, '2021_03_30_171045_create_likesdocument', 1),
(8, '2021_03_30_173318_create_permission_tables', 1),
(9, '2021_04_13_081732_create_slider_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `model_has_permissions`
--

INSERT INTO `model_has_permissions` (`permission_id`, `model_type`, `model_id`) VALUES
(1, 'App\\Models\\User', 1),
(2, 'App\\Models\\User', 1),
(3, 'App\\Models\\User', 1),
(4, 'App\\Models\\User', 1),
(5, 'App\\Models\\User', 1),
(6, 'App\\Models\\User', 1),
(7, 'App\\Models\\User', 1),
(8, 'App\\Models\\User', 1),
(9, 'App\\Models\\User', 1),
(10, 'App\\Models\\User', 1),
(11, 'App\\Models\\User', 1),
(12, 'App\\Models\\User', 1),
(12, 'App\\Models\\User', 2),
(12, 'App\\Models\\User', 3),
(13, 'App\\Models\\User', 1),
(13, 'App\\Models\\User', 2),
(13, 'App\\Models\\User', 3),
(14, 'App\\Models\\User', 1),
(14, 'App\\Models\\User', 2),
(14, 'App\\Models\\User', 3),
(15, 'App\\Models\\User', 1),
(15, 'App\\Models\\User', 2),
(15, 'App\\Models\\User', 3),
(16, 'App\\Models\\User', 1),
(17, 'App\\Models\\User', 1),
(17, 'App\\Models\\User', 2),
(17, 'App\\Models\\User', 3),
(18, 'App\\Models\\User', 1),
(18, 'App\\Models\\User', 2),
(18, 'App\\Models\\User', 3),
(19, 'App\\Models\\User', 1),
(19, 'App\\Models\\User', 2),
(19, 'App\\Models\\User', 3),
(20, 'App\\Models\\User', 1),
(20, 'App\\Models\\User', 2),
(20, 'App\\Models\\User', 3),
(21, 'App\\Models\\User', 1),
(22, 'App\\Models\\User', 1),
(23, 'App\\Models\\User', 1),
(24, 'App\\Models\\User', 1),
(25, 'App\\Models\\User', 1),
(26, 'App\\Models\\User', 1),
(27, 'App\\Models\\User', 1),
(28, 'App\\Models\\User', 1),
(29, 'App\\Models\\User', 1),
(30, 'App\\Models\\User', 1),
(30, 'App\\Models\\User', 2),
(30, 'App\\Models\\User', 3),
(30, 'App\\Models\\User', 4),
(31, 'App\\Models\\User', 1),
(31, 'App\\Models\\User', 2),
(31, 'App\\Models\\User', 3),
(31, 'App\\Models\\User', 4),
(32, 'App\\Models\\User', 1),
(32, 'App\\Models\\User', 2),
(32, 'App\\Models\\User', 3),
(32, 'App\\Models\\User', 4),
(33, 'App\\Models\\User', 1),
(33, 'App\\Models\\User', 2),
(33, 'App\\Models\\User', 3),
(33, 'App\\Models\\User', 4),
(34, 'App\\Models\\User', 1),
(34, 'App\\Models\\User', 4);

-- --------------------------------------------------------

--
-- Struktur dari tabel `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\Models\\User', 1),
(2, 'App\\Models\\User', 2),
(2, 'App\\Models\\User', 3),
(3, 'App\\Models\\User', 4);

-- --------------------------------------------------------

--
-- Struktur dari tabel `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'user-list', 'web', '2021-10-18 10:18:07', '2021-10-18 10:18:07'),
(2, 'user-create', 'web', '2021-10-18 10:18:07', '2021-10-18 10:18:07'),
(3, 'user-edit', 'web', '2021-10-18 10:18:07', '2021-10-18 10:18:07'),
(4, 'user-delete', 'web', '2021-10-18 10:18:08', '2021-10-18 10:18:08'),
(5, 'user-import-export', 'web', '2021-10-18 10:18:08', '2021-10-18 10:18:08'),
(6, 'kategori-list', 'web', '2021-10-18 10:18:08', '2021-10-18 10:18:08'),
(7, 'kategori-create', 'web', '2021-10-18 10:18:08', '2021-10-18 10:18:08'),
(8, 'kategori-edit', 'web', '2021-10-18 10:18:08', '2021-10-18 10:18:08'),
(9, 'kategori-delete', 'web', '2021-10-18 10:18:08', '2021-10-18 10:18:08'),
(10, 'kategori-export', 'web', '2021-10-18 10:18:08', '2021-10-18 10:18:08'),
(11, 'kategori-import', 'web', '2021-10-18 10:18:08', '2021-10-18 10:18:08'),
(12, 'dokumen-list', 'web', '2021-10-18 10:18:08', '2021-10-18 10:18:08'),
(13, 'dokumen-create', 'web', '2021-10-18 10:18:08', '2021-10-18 10:18:08'),
(14, 'dokumen-edit', 'web', '2021-10-18 10:18:08', '2021-10-18 10:18:08'),
(15, 'dokumen-delete', 'web', '2021-10-18 10:18:08', '2021-10-18 10:18:08'),
(16, 'dokumen-export', 'web', '2021-10-18 10:18:08', '2021-10-18 10:18:08'),
(17, 'ebook-list-admin', 'web', '2021-10-18 10:18:08', '2021-10-18 10:18:08'),
(18, 'ebook-delete-admin', 'web', '2021-10-18 10:18:08', '2021-10-18 10:18:08'),
(19, 'ebook-create-admin', 'web', '2021-10-18 10:18:08', '2021-10-18 10:18:08'),
(20, 'ebook-edit-admin', 'web', '2021-10-18 10:18:08', '2021-10-18 10:18:08'),
(21, 'ebook-edit-superadmin', 'web', '2021-10-18 10:18:08', '2021-10-18 10:18:08'),
(22, 'ebook-list-superadmin', 'web', '2021-10-18 10:18:08', '2021-10-18 10:18:08'),
(23, 'ebook-delete-superadmin', 'web', '2021-10-18 10:18:08', '2021-10-18 10:18:08'),
(24, 'komentar-list', 'web', '2021-10-18 10:18:08', '2021-10-18 10:18:08'),
(25, 'komentar-delete', 'web', '2021-10-18 10:18:08', '2021-10-18 10:18:08'),
(26, 'komentar-export', 'web', '2021-10-18 10:18:08', '2021-10-18 10:18:08'),
(27, 'preview-admin', 'web', '2021-10-18 10:18:08', '2021-10-18 10:18:08'),
(28, 'preview-list', 'web', '2021-10-18 10:18:08', '2021-10-18 10:18:08'),
(29, 'dashboard-admin', 'web', '2021-10-18 10:18:08', '2021-10-18 10:18:08'),
(30, 'profile', 'web', '2021-10-18 10:18:08', '2021-10-18 10:18:08'),
(31, 'reference', 'web', '2021-10-18 10:18:08', '2021-10-18 10:18:08'),
(32, 'like', 'web', '2021-10-18 10:18:08', '2021-10-18 10:18:08'),
(33, 'dashboard-user', 'web', '2021-10-18 10:18:08', '2021-10-18 10:18:08'),
(34, 'kumpulan-buku', 'web', '2021-10-18 10:18:08', '2021-10-18 10:18:08');

-- --------------------------------------------------------

--
-- Struktur dari tabel `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'pusdiklat', 'web', '2021-10-18 10:18:08', '2021-10-18 10:18:08'),
(2, 'updl', 'web', '2021-10-18 10:18:08', '2021-10-18 10:18:08'),
(3, 'siswa', 'web', '2021-10-18 10:18:08', '2021-10-18 10:18:08');

-- --------------------------------------------------------

--
-- Struktur dari tabel `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `slider`
--

CREATE TABLE `slider` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `gambar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `status`
--

CREATE TABLE `status` (
  `id_status` bigint(20) UNSIGNED NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `status`
--

INSERT INTO `status` (`id_status`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Di Terima', '2021-10-18 10:18:07', NULL),
(2, 'Di Tolak', '2021-10-18 10:18:07', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_auth`
--

CREATE TABLE `user_auth` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `full_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar_file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_type_id` bigint(20) DEFAULT NULL,
  `user_entity_id` bigint(20) DEFAULT NULL,
  `user_key` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `login_at` datetime DEFAULT NULL,
  `modified_at` datetime NOT NULL DEFAULT current_timestamp(),
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `user_auth`
--

INSERT INTO `user_auth` (`id`, `full_name`, `avatar_file`, `email`, `username`, `password`, `user_type_id`, `user_entity_id`, `user_key`, `login_at`, `modified_at`, `created_at`, `updated_at`) VALUES
(1, 'superadmin', NULL, 'superadmin@plnpusdiklat.com', 'superadmin', '$2y$10$xM6NxZCs2uXuBY4kK0DfCOv01hw8DbAR9DgaH6TYxjpw6FdOy8V8C', NULL, NULL, NULL, NULL, '2021-10-19 00:18:08', '2021-10-18 10:18:08', '2021-10-18 10:18:08'),
(2, 'wahyu', NULL, 'administrator@plnpusdiklat.com', 'wahyu', '$2y$10$qOfMazsxbSY3ucPjwTefyezWhzC2yAJuGcR.BQKYeEzv02.niD9Vy', NULL, NULL, NULL, NULL, '2021-10-19 00:18:08', '2021-10-18 10:18:08', '2021-10-18 10:18:08'),
(3, 'admin', NULL, 'admin@plnpusdiklat.com', 'admin', '$2y$10$TlKn60CGZ426d2FkDj9nROSYI8TYm47Ry21AIeikdxkKn4SH6q2oO', NULL, NULL, NULL, NULL, '2021-10-19 00:18:08', '2021-10-18 10:18:08', '2021-10-18 10:18:08'),
(4, 'andi', NULL, 'andi@plnpusdiklat.com', 'andi', '$2y$10$vbAEAFtkgfTRTdxkemxqA.BNbLsWUaWgDwZlSbjIMMY4DVjqY9uVq', NULL, NULL, NULL, NULL, '2021-10-19 00:18:08', '2021-10-18 10:18:08', '2021-10-18 10:18:08');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `document`
--
ALTER TABLE `document`
  ADD PRIMARY KEY (`id`),
  ADD KEY `document_id_kategori_foreign` (`id_kategori`);

--
-- Indeks untuk tabel `ebook`
--
ALTER TABLE `ebook`
  ADD PRIMARY KEY (`id_ebook`),
  ADD KEY `ebook_id_kategori_foreign` (`id_kategori`);

--
-- Indeks untuk tabel `forum`
--
ALTER TABLE `forum`
  ADD PRIMARY KEY (`id`),
  ADD KEY `forum_dokumen_id_foreign` (`dokumen_id`);

--
-- Indeks untuk tabel `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indeks untuk tabel `kategori_type`
--
ALTER TABLE `kategori_type`
  ADD PRIMARY KEY (`kategori_type_id`);

--
-- Indeks untuk tabel `likesdocument`
--
ALTER TABLE `likesdocument`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indeks untuk tabel `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indeks untuk tabel `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indeks untuk tabel `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indeks untuk tabel `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indeks untuk tabel `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id_status`);

--
-- Indeks untuk tabel `user_auth`
--
ALTER TABLE `user_auth`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `document`
--
ALTER TABLE `document`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `ebook`
--
ALTER TABLE `ebook`
  MODIFY `id_ebook` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `forum`
--
ALTER TABLE `forum`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id_kategori` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT untuk tabel `kategori_type`
--
ALTER TABLE `kategori_type`
  MODIFY `kategori_type_id` int(122) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `likesdocument`
--
ALTER TABLE `likesdocument`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT untuk tabel `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `slider`
--
ALTER TABLE `slider`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `status`
--
ALTER TABLE `status`
  MODIFY `id_status` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `user_auth`
--
ALTER TABLE `user_auth`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `document`
--
ALTER TABLE `document`
  ADD CONSTRAINT `document_id_kategori_foreign` FOREIGN KEY (`id_kategori`) REFERENCES `kategori` (`id_kategori`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `ebook`
--
ALTER TABLE `ebook`
  ADD CONSTRAINT `ebook_id_kategori_foreign` FOREIGN KEY (`id_kategori`) REFERENCES `kategori` (`id_kategori`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `forum`
--
ALTER TABLE `forum`
  ADD CONSTRAINT `forum_dokumen_id_foreign` FOREIGN KEY (`dokumen_id`) REFERENCES `document` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
